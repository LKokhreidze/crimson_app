class AddKmlToMicroposts < ActiveRecord::Migration
  def change
    add_column :microposts, :kml, :string
  end
end
