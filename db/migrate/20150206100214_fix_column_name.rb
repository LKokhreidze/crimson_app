class FixColumnName < ActiveRecord::Migration
  def change
    rename_column :microposts, :type, :tour_type
  end
end