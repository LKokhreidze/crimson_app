Rails.application.routes.draw do

  # REST for sessions
  match '/signin', to: 'sessions#new', via: :get
  match '/sessions', to: 'sessions#create', via: :post
  match '/signout', to: 'sessions#destroy', via: :delete
  match '/relationships/followers/count', to: 'relationships#refresh_followers', via: :get

  resources :microposts, only: [:create, :destroy]
  resources :relationships, only: [:create, :destroy]

  resources :users do
    member do
      get :following, :followers
    end
  end
  # match '/users', to: 'users#index', via: :get
  # match '/users/:id', to: 'users#show', via: :get, as: :user do
  #   member do
  #     match '/following', to: 'users#following', via: :get
  #     match '/followers', to: 'users#followers', via: :get
  #   end
  # end
  # match '/users/:id', to: 'users#destroy', via: :delete, as: :delete_user
  # match '/users' => 'users#create', via: :post
  match '/signup', to: 'users#new', via: :get
  # match '/users/:id/edit', to: 'users#edit', via: :get, as: :edit_user

  # Routes for follow


  # Routes for Static Pages
  root to: 'static_pages#home'
  match '/about', to: 'static_pages#about', via: :get, as: :about
  match '/help', to: 'static_pages#help', via: :get, as: :help
  match '/contact', to: 'static_pages#contact', via: :get, as: :contact

  match '/map', to: 'static_pages#map', via: :get

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
