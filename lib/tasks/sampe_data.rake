namespace :db do
  desc 'Fill in Database'
  task populate: :environment do
    make_users
  end
end

def make_users
  user = User.create!(name: 'Levani Kokhreidze', email: 'lkokhreidze@example.org',
                      password: 'foobar', password_confirmation: 'foobar')
  user.microposts.build(content: 'Hiking-touristic route to the Kareli and Borjomi municipality. Within the tour frames you will be able to travel to Dzama and Gujareti gorges to visit architectural-historical and nature monuments, to enjoy picturesque landscapes and experience an adventure.',
                        image: 'http://lamerwebmaster.dsl.ge/developer/TourImg/Kvara.jpg',
                        name: 'Dzama and Gujareti',
                        tour_type: 'Foot',
                        kml: 'http://lamerwebmaster.dsl.ge/developer/KML/kml')
  user.microposts.build(content: 'The three-day camping trip to the lake Kahisi (Borjomi municipality) will give the opportunity to visit the historic corner of Georgia Mesheti resort Borjomi, historic area Tory, enjoy the beautiful scenery - lakes, situated on a plateau Dabadzveli, inspect Green monastery and return home with a great impression',
                        image: 'http://lamerwebmaster.dsl.ge/developer/TourImg/Kakhisi.jpg',
                        name: 'Hike to Lake Kahisi',
                        tour_type: 'Foot',
                        kml: 'http://lamerwebmaster.dsl.ge/developer/KML/kml1')
  user.microposts.build(content: 'One-day / two-day car trip to Historical Ertso - Tianeti area of the park. On the way you can see Mamkoda , Tskhvarichamia , Sabadura Forest River Iori. Visit the historic monuments - St. Mr. King , and they were referred zhaletisa monasteries, organize a picnic and enjoy the beautiful nature or can taste  local traditional dishes ( Khinkali, barbecue ).',
                        image: 'http://lamerwebmaster.dsl.ge/developer/TourImg/Tianeti.jpg',
                        name: 'Ertso Tianeti',
                        tour_type: 'Vehicle',
                        kml: 'http://lamerwebmaster.dsl.ge/developer/KML/kml2')
  user.microposts.build(content: 'Tarty city tour. Find interesting places',
                        image: 'http://www.tntmagazine.com/media/content/_master/39135/images/estonia-kissing-students.jpg',
                        name: 'Tartu Tour',
                        tour_type: 'City Tour',
                        kml: 'http://lamerwebmaster.dsl.ge/developer/KML/kml6')
  user.toggle!(:admin)
end