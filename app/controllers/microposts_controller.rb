class MicropostsController < ApplicationController
  require 'nokogiri'
  require 'open-uri'
  require 'net/ftp'
  require 'socket'
  require 'websocket/dr'
  before_filter :signed_in_user
  before_filter :correct_user, only: :destroy

  def create
    ftp = configure_ftp
    kml_source = params[:micropost][:kml]
    img_source = params[:micropost][:image]
    increment_id = get_incremented_id
    img_name = "img#{increment_id}.jpg"

    if !params[:micropost][:kml].empty?
      params[:micropost][:kml] = "http://prof.univ.kiev.ua/app_temp/kml#{increment_id}"
    end
    if !params[:micropost][:image].nil?
      params[:micropost][:image] = "http://prof.univ.kiev.ua/app_temp/#{img_name}"
    end

    @micropost = current_user.microposts.build(micropost_params)
    tours = modify_xml(increment_id, @micropost.name, @micropost.content, @micropost.tour_type, @micropost.kml, @micropost.image)
    if @micropost.save
      store_img(ftp, img_source, img_name)
      store_on_ftp(ftp, tours.to_s, 'test.xml')
      store_on_ftp(ftp, kml_source, "kml#{increment_id}")
      flash[:success] = 'new post added'
      redirect_to root_path
    else
      @feed_items = []
      render 'static_pages/home'
    end
  end

  def destroy
    @micropost.destroy
    flash[:success] = 'post destroyed';
    redirect_to root_path
  end

  private
    def micropost_params
      params.require(:micropost).permit(:content, :name, :tour_type, :kml, :image)
    end

    def correct_user
      @micropost = current_user.microposts.find_by_id(params[:id])
      redirect_to root_path if @micropost.nil?
    end
end