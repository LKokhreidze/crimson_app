class SessionsController < ApplicationController
  def new

  end

  def create
    user = User.find_by_email(params[:session][:email])
    if user && user.authenticate(params[:session][:password])
      signin_user user
      redirect_back_or user
    else
      flash.now[:error] = 'email/password invalid'
      render 'sessions/new'
    end

  end

  def destroy
    sign_out
    redirect_to root_path
  end
end