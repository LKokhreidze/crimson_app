class UsersController < ApplicationController
  before_filter :signed_in_user, only: [:index, :edit, :destroy, :update, :followers, :following]
  before_filter :correct_user, only: [:edit, :update]
  before_filter :admin_user, only: [:destroy]

  def index
    @users = User.paginate(page: params[:page], per_page: 10)
  end

  def new
    @user = User.new
  end

  def show
    @user = find_user(params[:id])
    @micropost = @user.microposts.paginate(page: params[:page])
  end

  def create
    @user = User.new(user_params)
    if @user.valid? then
      flash[:success] = 'Welcome to My App!'
      @user.save
      signin_user @user
      redirect_to user_path(@user)
    else
      render 'users/new'
    end
  end

  def edit
  end

  def update
    @user = find_user(params[:id])
    if @user.update_attributes(user_params)
      signin_user @user
      flash[:success] = "Profile for '#{@user.name}' was updated."
      redirect_to @user
    else
      render 'users/edit'
    end
  end

  def destroy
    User.find(params[:id]).destroy
    flash[:success] = 'User deleted'
    redirect_to users_path
  end

  def following
    @title = 'Following'
    @user = User.find(params[:id])
    @users = @user.followed_users.paginate(page: params[:page])
    render 'show_follow'
  end

  def followers
    @title = 'Followers'
    @user = User.find(params[:id])
    @users = @user.followers.paginate(page: params[:page])
    render 'show_follow'
  end

  private
    def user_params
      params.require(:user).permit(:name, :email, :password, :password_confirmation, :microposts)
    end

    def find_user(id)
      User.find(id)
    end

    def correct_user
      @user = find_user(params[:id])
      redirect_to root_path unless current_user?(@user )
    end

    def admin_user
      redirect_to root_path unless current_user.admin?
    end
end
