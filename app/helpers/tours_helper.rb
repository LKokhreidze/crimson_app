module ToursHelper
  def store_on_ftp(ftp, file_string, name)
    io_file = StringIO.new(file_string)
    ftp.storbinary("STOR #{name}", io_file, Net::FTP::DEFAULT_BLOCKSIZE)
    io_file.close()
  end

  def modify_xml(_id, _name, _desc, _tour_type, _tour_url, _tour_img)
    tour_info = Nokogiri::XML(open('http://prof.univ.kiev.ua/app_temp/test.xml'))
    tours = tour_info.root
    tour = Nokogiri::XML::Node.new('Tour', tours)
    tour.set_attribute('ID', _id)
    name = Nokogiri::XML::Node.new('Name', tour)
    type = Nokogiri::XML::Node.new('Type', tour)
    desc = Nokogiri::XML::Node.new('Description', tour)
    tour_url = Nokogiri::XML::Node.new('TourUrl', tour)
    tour_img = Nokogiri::XML::Node.new('TourImg', tour)
    name.content = _name
    desc.content = _desc
    type.content = _tour_type
    tour_url.content = _tour_url
    tour_img.content = _tour_img
    tour << name
    tour << desc
    tour << type
    tour << tour_url
    tour << tour_img
    tours << tour
    tours
  end

  def configure_ftp
    ftp = Net::FTP.new('prof.univ.kiev.ua')
    ftp.login(user = 'wwwprof', password = 'KEQ2j2BNtsqfaewf')
    ftp.passive = true
    ftp.chdir('www/app_temp')
    ftp
  end

  def get_incremented_id
    tour_info = Nokogiri::XML(open('http://prof.univ.kiev.ua/app_temp/test.xml'))
    all_tours = tour_info.xpath("//Tour")
    last_tour = all_tours[all_tours.length - 1]
    increment_id = Integer(last_tour.attribute("ID").to_s) + 1
    increment_id
  end

  def store_img(ftp, img_source, img_name)
    if (!img_source.nil?)
      img = img_source.tempfile
      ftp.storbinary("STOR #{img_name}", img, Net::FTP::DEFAULT_BLOCKSIZE)
    end
  end
end