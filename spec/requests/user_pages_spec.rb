require 'spec_helper'

describe 'UserPages' do
  subject { page }

  describe 'sign up page' do
      before { visit signup_path }
      it { should have_selector('h1', :text => 'Sign Up Page') }
      it { should have_selector('title', :text => full_title('Sign Up')) }
  end

  describe 'profile page' do
    let(:foo) { User.create(name: Faker::Name.name, email: Faker::Internet.email,
                            password: 'foobar', password_confirmation: 'foobar') }

    before { visit user_path(foo) }

    it { should have_selector('title', :text => full_title(foo.name)) }
    it { should have_selector('h1', :text => foo.name) }
  end

  describe 'sign up' do
    before { visit signup_path }
    let(:submit) { 'Create My Account!' }

    describe 'invalid info' do
      it 'should not create a user' do
        expect { click_button submit }.not_to change(User, :count)
      end
    end

    describe 'valid info' do
      let(:current_email) { Faker::Internet.email }
      before {
        fill_in 'Name',         with: 'lk'
        fill_in 'Email',        with: current_email
        fill_in 'Password',     with: 'foobar'
        fill_in 'Confirmation', with: 'foobar'
      }

      it 'should save user' do
        expect { click_button submit }.to change(User, :count).by(1)
      end

      describe 'after saving the user' do
        before { click_button submit }
        let(:user) { User.find_by_email(current_email) }

        it { should have_selector('title', text: user.name) }
        it { should have_selector('div.alert.alert-success', text: 'Welcome') }
      end
    end
  end

  describe 'edit' do

    local_email = Faker::Internet.email
    User.create(name: Faker::Name.name, email: local_email,
                password: 'foobar', password_confirmation: 'foobar')
    let(:user) { User.find_by_email(local_email) }

    describe 'page' do
      before do
        visit signin_path
        fill_in "Email", with: user.email
        fill_in "Password", with: user.password
        click_button 'Sign In'
        visit edit_user_path(user)
      end

      it { should have_selector('h1', text: 'Update Profile') }
      it { should have_selector('title', text: "Edit Profile | #{user.name}") }
      it { should have_link('Change', href: 'http://gravatar.com/emails')}
    end

    describe 'with invalid info' do
      before { click_button 'Save Changes' }

      it { should have_content('error') }
    end

    describe 'with valid info' do
      let(:new_name) { 'New Name' }
      let(:new_email) { 'new@mail.com' }
      before do
        visit signup_path
        fill_in 'Name',     with: new_name
        fill_in 'Email',    with: new_email
        fill_in 'Password', with: user.password
        fill_in 'Confirm',  with: user.password
        click_button 'Save Changes'
      end
      it { should have_selector('title', text: new_name) }
      # it { should have_link('Sign Out', href: signout_path)}
      it { should have_selector('div.alert.alert-success') }
      specify { user.reload.name.should == new_name }
    end
  end
end