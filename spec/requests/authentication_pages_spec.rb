require 'spec_helper'

describe "AuthenticationPages" do
  subject { page }
  describe 'sign in page' do
    before { visit signin_path }
    it { should have_selector('h1',    text: 'Sign In')}
    it { should have_selector('title', text: full_title('Sign In'))}
  end

  describe 'sign in' do
    before { visit signin_path }
    let(:signin) { 'Sign In' }
    describe 'with invalid sign in' do
      before { click_button signin }
      it { should have_selector('title', text: full_title('Sign In')) }
      it { should have_selector('div.alert.alert-error', text: 'email/password') }

      describe 'after visiting another page' do
        before { click_link 'Home' }
        it { should_not have_selector('div.alert.alert-error') }
      end
    end

    describe 'with valid information' do
      let(:user) { User.create(email: 'foobar@foo.com',
                               name: 'Levani Kokhreidze',
                               password: 'foobar',
                               password_confirmation: 'foobar') }
      before do
        fill_in 'Email', with: user.email
        fill_in 'Password', with: user.password
        click_button signin
      end

      it { should have_selector('title',   text: full_title(user.name)) }
      it { should have_link('Profile', href: user_path(User.find_by_email('foobar@foo.com'))) }
      it { should have_link('Sign Out',    href: signout_path) }
      it { should_not have_link('Sign In', href: signin_path) }

      describe 'followed by sign out' do
        before { click_link 'Sign Out' }
        it { should have_link('Sign In') }
      end
    end
  end
end