require 'spec_helper'

describe User do
  before {
    @user = User.new(email:                 Faker::Internet.email,
                     name:                  Faker::Name.name,
                     password:              'foobar',
                     password_confirmation: 'foobar')
  }

  subject { @user }

  it { should respond_to(:name) }
  it { should respond_to(:email) }
  it { should respond_to(:password_digest) }
  it { should respond_to(:password) }
  it { should respond_to(:password_confirmation) }
  it { should respond_to(:authenticate) }
  it { should respond_to(:remember_token) }
  it { should be_valid }

  describe 'when name not present' do
    before { @user.name = '' }

    it { should_not  be_valid }
  end

  describe 'when email not preset' do
    before { @user.email = '' }
    it { should_not be_valid }
  end

  describe 'when name is too long' do
    before { @user.name = 'a' * 51 }
    it { should_not be_valid }
  end

  describe 'when email validation fails' do
    it 'should be invalid' do
      addresses = %w[user@foo,com user_at.com test@foo.]
      addresses.each { |invalid_address|
        @user.email = invalid_address
        @user.should_not be_valid
      }
    end
  end

  describe 'when email validation is correct' do
    it 'should be valid' do
      addresses = %w[user@foo.com user_at@gmail.com test@foo.org]
      addresses.each { |valid_address|
        @user.email = valid_address
        @user.should be_valid
      }
    end
  end

  describe 'when email already used' do
    before do
      user_with_same_email_address = @user.dup
      user_with_same_email_address.save
    end

    it { should_not be_valid }
  end

  describe 'when there is no password' do
    before { @user.password = @user.password_confirmation = ' ' }
    it { should_not be_valid }
  end

  describe 'when passwords does not match' do
    before { @user.password_confirmation = 'noo!' }
    it { should_not be_valid }
  end

  describe 'when password confirmation is nil' do
    before { @user.password_confirmation = nil }
    it { should_not be_valid }
  end

  describe 'when password length too short' do
    before { @user.password = @user.password_confirmation = 'i' * 5 }
    it { should_not be_valid }
  end

  describe 'return authenticate' do
    before { @user.save }
    let(:found_user) { User.find_by_email(@user.email) }

    # describe 'when password is valid' do
    #   it { should == found_user.authenticate(@user.password)}
    # end

    describe 'when password is not valid' do
      let(:is_authenticated) { found_user.authenticate('yo man!') }
      it { should_not == :is_authenticated }
      specify { is_authenticated.should be_false }
    end
  end

end
